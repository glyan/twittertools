import threading
import time
from os import system

import pandas
import tweepy

"""
def explore(method, *args, **kwargs):
    token = ''
    start = True
    responses = []
    try:
        for response in tweepy.Paginator(method, *args, **kwargs):
            start = False
            try:
                token = response.meta['next_token']
            except KeyError:
                token = ''

            responses += response
            # print(response.meta)
            print('token:', token)
    except TooManyRequests:
        # Restart the process if
        if start:
            print("Timeout not finished yet, re-attempting to fetch data")
            if not Utils.sleeper.is_alive():
                Utils.sleeper = Slipper(Utils.TIMEOUT_QUERY_SECONDS)
                Utils.sleeper.start()
            #while Utils.sleeper.is_alive():
                #time.sleep(1)
            explore(method, *args, **kwargs)
            # return responses + explore(method, *args, **kwargs)
        else:
            print("crawl")
            while token != '':
                Utils.sleeper = Slipper(Utils.TIMEOUT_QUERY_SECONDS)
                Utils.sleeper.start()
               # while Utils.sleeper.is_alive():
                   # time.sleep(1)
                try:
                    for response in tweepy.Paginator(method, *args, **kwargs, pagination_token=token):
                        try:
                            token = response.meta['next_token']
                        except KeyError:
                            token = ''
                        responses += response
                        # print(response.meta)
                        print('token:', token)
                except TooManyRequests:
                    print("Maximum number of queries reached.")
                    # print(token)

    return responses
"""


def forEach(aFunction, anIterable):
    for eachElement in anIterable:
        aFunction(eachElement)


class Slipper(threading.Thread):

    def getRemainingTime(self):
        return int(self.sleepMinutes)

    def slip(self):
        self.cpt = 0
        self.sleepMinutes = int(self.sleep_time / 60)
        seconds = int((self.sleep_time / 60 - self.sleepMinutes) * 60)
        print(self.sleepMinutes, "minutes", seconds, "seconds until end of timeout")
        self.cpt = 60 - seconds
        while self.cpt < self.sleep_time:
            if self.cpt > 0 and self.cpt % 60 == 0:
                print(self.sleepMinutes, "minutes until end of timeout")
                self.sleepMinutes -= 1
            time.sleep(1)
            self.cpt += 1
            # system('clear')

    def __init__(self, sleep_time, sleepMinutes=0, cpt=0):
        threading.Thread.__init__(self)
        self.sleep_time = sleep_time
        self.cpt = cpt
        self.sleepMinutes = sleepMinutes

    def run(self):
        self.slip()


def slip(sleep_time):
    cpt = 0
    print(sleep_time / 60, "minutes until next try")
    sleepMinutes = int(sleep_time / 60)
    while cpt < sleep_time:
        # logging.info(sleep_time-cpt, "seconds until next try")
        if cpt > 0 and cpt % 60 == 0:
            print(sleepMinutes, "minutes until next try")
            sleepMinutes -= 1
        time.sleep(1)
        cpt += 1
        system('clear')


"""
Contains useful tools for Twitter API
and misc. configuration variables / constants.
"""


class Utils:
    # tweets_db = pandas.read_parquet('ifNull.parquet')
    sleeper = Slipper(0)
    TIMEOUT_QUERY_SECONDS: int = 905

    # THIS IS THE TWITTER API TOKEN
    # When authenticating requests to the Twitter API v2 endpoints, you must use keys and tokens from a Twitter developer App that is attached to a Project. You can create a project via the developer portal.
    MY_BEARER_TOKEN = "AAAAAAAAAAAAAAAAAAAAAOnlcgEAAAAAOElCZ5FJVYzYZQ%2FQzqLZqY7ZZkk%3DC3RRygAKqiwTFQORjpoMzoQ3QwFmVc7rd1b8utPirW00dDhK0m"


    client = tweepy.Client(bearer_token=MY_BEARER_TOKEN, wait_on_rate_limit=True)



    user_fields = ["created_at", "description", "entities", "id", "location", "name", "pinned_tweet_id",
                   "profile_image_url", "protected", "public_metrics", "url", "username", "verified", "withheld"]
    media_fields = ['preview_image_url', 'url']
    tweet_fields = ['attachments', 'author_id', 'context_annotations', 'conversation_id', 'created_at', 'entities',
                    'geo', 'id', 'in_reply_to_user_id', 'lang', 'non_public_metrics', 'public_metrics',
                    'organic_metrics', 'promoted_metrics', 'possibly_sensitive', 'referenced_tweets', 'reply_settings',
                    'source', 'text', 'withheld']

    # set of fraudulous accounts for tweet social marking
    fraudulous = {'france_soir', 'BVoltaire', 'WikistrikeW', 'Planetes360', 'reseau_internat', 'Le_Salon_Beige_',
                  'beige_salon', 'Algpatriotique', 'F_Desouche', 'idrissaberkane', 'DIVIZIO1', 'jjcrevecoeur',
                  't_casasnovas', 'ThierryRegenere', 'MartineWonner', 'ReinfoCovidOff'}

    # set of valuable accounts for tweet social marking
    checker = {'conspiration', 'l_extracteur', 'DEFAKATOR_Off', 'DeBunKerEtoiles', 'TroncheBiais', 'fakemedecine',
               'DEFACTO_UE', 'CheckNewsfr', 'ArteDesintox'}
