# Twittertools

This repo. contains some utils to do miscellaneous actions over twitter API v2 using tweepy and to extract information from this data

Attached data is deployed at ``/nfs/stk-deepfakes.irisa.fr/data/teams/deepfakes/deepfakes/fake_tweets_data``

## datasets in fake_tweets_data

### Fake_ukraine
This folder contains tweets about fake news about the russo-ukrainian conflict that were collected by Vincent Claveau as compressed gzip files containing json responses from twitter API
### Legacy_full
It contains parquet files of the Fake_ukraine dataset transformed by `legacy.py`
````
                        id tweet_type                      created_at  ...     media_key                                          media_url   type
0      1538641295429738497       ORIG  Sun Jun 19 21:54:06 +0000 2022  ...           NaN                                               None   None
1      1538641296386056192         QT  Sun Jun 19 21:54:06 +0000 2022  ...  1.538439e+18    https://pbs.twimg.com/media/FVmh0bUWYAUnEqv.jpg  photo
2      1538641296889348096         QT  Sun Jun 19 21:54:06 +0000 2022  ...  1.538439e+18    https://pbs.twimg.com/media/FVmh0bUWYAUnEqv.jpg  photo
3      1538641298143453184         RT  Sun Jun 19 21:54:06 +0000 2022  ...  1.538211e+18  https://pbs.twimg.com/ext_tw_video_thumb/15382...  video
4      1538641304011395074       ORIG  Sun Jun 19 21:54:08 +0000 2022  ...           NaN                                               None   None
...                    ...        ...                             ...  ...           ...                                                ...    ...
49995  1538894402906738689         RT  Mon Jun 20 14:39:51 +0000 2022  ...           NaN                                               None   None
49996  1538894404181688320         QT  Mon Jun 20 14:39:51 +0000 2022  ...           NaN                                               None   None
49997  1538894407084171264       ORIG  Mon Jun 20 14:39:52 +0000 2022  ...           NaN                                               None   None
49998  1538894411672715264         RT  Mon Jun 20 14:39:53 +0000 2022  ...           NaN                                               None   None
49999  1538894411609821185       ORIG  Mon Jun 20 14:39:53 +0000 2022  ...           NaN                                               None   None
````

### qts_rt-qt-ids

It contains the id of tweets from legacy_full that are quotes (QT) or retweets(RT) along with the id of the resp quoted or retweeted tweet.

````
                        id             rt_qt_id
0      1531970691083411456  1531865308910297088
1      1499693055645040641  1499637454449659910
2      1530107878048247808  1530098208994742272
3      1498938136424030211  1498937540140834818
4      1499201622962053125  1499117630791335938
...                    ...                  ...
37080  1547229709003530241  1547083641548857350
37081  1499715087053082629  1499447622150631425
37082  1499716358619242496  1499710253700718596
37083  1518701672708595725  1518502353120411649
37084  1513893908190793728  1498758925256118283
````

### marks

**c.f slides 31-39 of project.pdf** for information about the idea of social marking

![](marks.png)

Contains parquet files of some tweets of legacy_full for which the social marking was run

````
                  created_at                                        description                                           entities                   id  ... checks mark  kind             tweet_id
0  2019-11-10 13:49:37+00:00  Matfem sexneg  pour la lesbianisation des mass...  {'description': {'hashtags': None, 'mentions':...  1193525942301405184  ...      2  2.0    RT  1501820450330103812
1  2018-05-20 13:15:18+00:00                               pp avec @FunnyRedrum  {'description': {'hashtags': None, 'mentions':...   998190427625967618  ...      1  1.0    RT  1501820450330103812
2  2017-09-08 21:52:12+00:00  lizi/pride & prejudice fan account/your local ...  {'description': None, 'url': {'urls': [{'displ...   906273992092385280  ...      0  0.0    RT  1501820450330103812
3  2013-04-04 17:01:29+00:00  ☭ ⚢. 28. sci-fi nerd. militante @NPA_Rennes 📢,...  {'description': {'hashtags': None, 'mentions':...           1327382275  ...      0  0.0    RT  1501820450330103812
4  2016-07-18 08:32:26+00:00                                                                                                  None   754956984638705664  ...      0  0.0    RT  1501820450330103812
..                       ...                                                ...                                                ...                  ...  ...    ...  ...   ...                  ...
95 2017-01-11 17:22:09+00:00                                                                                                  None   819232943008845825  ...      0  0.0    <3  1501820450330103812
96 2010-09-09 09:04:41+00:00  Le seul Système compatible avec la vie et la d...                                               None            188671648  ...      0  0.0    <3  1501820450330103812
97 2014-02-13 13:09:33+00:00  Membre du NPA\n"Dans le travail pour l’incerta...                                               None           2341967305  ...      0  0.0    <3  1501820450330103812
98 2019-03-23 19:22:53+00:00  🌋 communisme, crippling depression 🌋\n\nMa vie...                                               None  1109536007643414528  ...      1  1.0    <3  1501820450330103812
99 2015-09-15 21:16:54+00:00  pixel art and stuff. eventual nudity in my pix...                                               None           3667885174  ...      0  0.0    <3  1501820450330103812
````

### qts

Contains only quotes tweets from legacy_full
````
                          id tweet_type                                               text                                          full_text
0        1498274252734316544         QT  Il me semble qu'elle a très peu d'expérience a...  La si redoutable armée russe est en réalité bi...
1        1498274323362103300         QT  cette femme s'appele christelle neant.Journali...  Puisque RT France a supprimé en catimini cette...
2        1498274613591224322         QT  RT @anamitethau: Oui il faut continuer #Macron...  80% des promesses de #Macron en 2017 sont tenu...
3        1498274685548650496         QT  RT @Avocatweet: Conseil aux délégués ukrainien...  [ 🇷🇺 RUSSIE | 🇺🇦 UKRAINE ]\n\n🔸«En Biélorussie...
4        1498274698060345348         QT              Tu vas la fermer ta grande bouche !!!  80% des promesses de #Macron en 2017 sont tenu...
...                      ...        ...                                                ...                                                ...
2230719  1549386934530736133         QT  @cdion @BFMTV 'tain ... c'est vous dans l'itw ...  Énorme.\nC'est guerre climatique et pas un réc...
2230720  1549387831130333184         QT  RT @Scipionista: Footballeur  \nInfectiologue ...  @florentderue @todayitsfoot Il fait chaud en j...
2230721  1549399935682056194         QT  Un propagandiste du Kremlin sur une chaîne de ...  #BrunetEtCie Kochko est un propagandiste missi...
2230722  1549410104126177283         QT  Les priorités du pouvoir ne sont indubitableme...  Pas d’argent pour renouveler la flotte des Can...
2230723  1549411047081218050         QT  La guerre que c’est alors qu’on pourrait juste...  TOURNOI DES ANIMÉS | FINALE\nDeux tournois, de...
````


### tweets_imgs

Contains images for legacy tweets contained by folders named by tweets ids. Media are gathered using `notebooks/getMedia.ipynb`